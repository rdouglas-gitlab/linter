# frozen_string_literal: true

module Linter
  class PronounAssociation < BaseAssociation
    FULL_WORD = true

    def self.wordlists
      file_path = File.join(__dir__,'../../data/pronoun_association_wordlist.yml')
      @wordlists ||= YAML.load_file(file_path)
    end

    private

    def self.calculate_trend(result)
      return 'masculine-coded' if result.masculine_coded_word_counts.values.sum.positive?

      return 'feminine-coded' if result.feminine_coded_word_counts.values.sum.positive?

      'neutral'
    end
  end
end
