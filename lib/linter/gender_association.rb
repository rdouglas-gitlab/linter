# frozen_string_literal: true

module Linter
  class GenderAssociation < BaseAssociation
    FULL_WORD = false

    private

    def self.wordlists
      file_path = File.join(__dir__,'../../data/gender_association_wordlist.yml')
      @wordlists ||= YAML.load_file(file_path)
    end

    def self.calculate_trend(result)
      case result.feminine_coded_word_counts.values.sum - result.masculine_coded_word_counts.values.sum
      when 0
        'neutral'
      when 1..3
        'feminine-coded'
      when 3..Float::INFINITY
        'strongly feminine-coded'
      when -Float::INFINITY..0
        'strongly masculine-coded'
      else
        'masculine-coded'
      end
    end
  end
end
