# frozen_string_literal: true

require_relative 'linter/version'
require_relative 'linter/base_association'
require_relative 'linter/gender_association'
require_relative 'linter/pronoun_association'
require_relative 'linter/cli'
require_relative 'linter/misused_words'

require 'yaml'
require 'colorize'
require 'ostruct'

module Linter
  class Error < StandardError; end

  class << self
    def cli_analyze(file_name)
      Linter::CLI.analyze(file_name)
    end
  end
end
