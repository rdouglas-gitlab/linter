# frozen_string_literal: true

require 'spec_helper'

describe Linter::GenderAssociation do
  let(:text) { 'Collaborate closely Collaborate collaboratively with the affectionate . Analytics all the way.'}

  describe '.analyze' do
    it 'returns the feminine coded words' do
      result = described_class.analyze(text)
      expect(result.feminine_coded_word_counts).to eq({'affectionate' => 1, 'collaborate' => 2, 'collaboratively' => 1})
    end

    it 'returns the masculine coded words' do
      result = described_class.analyze(text)
      expect(result.masculine_coded_word_counts).to eq({'analytics' => 1})
    end

    it 'calculates a trend' do
      result = described_class.analyze(text)
      expect(result.trend).to eq('feminine-coded')
    end
  end
end
